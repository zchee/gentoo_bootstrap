#!/bin/bash

#-------------------------------------------------------------------------------
# build_gentoo.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

architecture=$1

# The region to install into
#region="us-east-1"
region=$2

# The security group to use. 22/tcp needs to be open
# Leave empty to have a group created
#group="default"
group=$3

# The ec2 key pair to use
# Leave empty to have a key created
#key="example"
key=$4

# The fully qualified path to private key of the ec2 key pair
# Leave empty to have a key created
#keyfile="$HOME/.ssh/example.pem"
keyfile=$5

instance_type="c1.medium"
remove_keypair="false"

function report_status() {
    echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: $@"
}

if [ -z $architecture ]; then
    echo "architecture not set"
    exit
elif [ $architecture != "i386" -a $architecture != "x86_64" ]; then
    echo "Unknown architecture: $architecture"
    echo "Valid architecture values: i386, x86_64"
    exit
fi

if [ $architecture = "x86_64" ]; then
    if [ -z $AMI_TYPE ]; then
        building="Gentoo 64 EBS"
    else
        building="Gentoo 64 EBS ($AMI_TYPE)"
    fi
    default_name="Gentoo_64-bit-EBS"
else
    if [ -z $AMI_TYPE ]; then
        building="Gentoo 32 EBS"
    else
        building="Gentoo 32 EBS ($AMI_TYPE)"
    fi
    default_name="Gentoo_32-bit-EBS"
fi

start_time=`date +%Y-%m-%dT%H:%M:%S`

if [[ $region == "" ]]; then
    region="us-east-1"
fi

if [[ $group == "" ]]; then
    report_status "set up group"
    group="gentoo-bootstrap"

    group_exists="`ec2-describe-group \
            --region $region \
            --filter group-name=$group \
            | wc -c`"

    if [ $group_exists -eq 0 ]; then
        ec2-create-group --region $region $group --description "Gentoo Bootstrap"
    fi

    ec2-authorize --region $region $group -P tcp -p 22 -s 0.0.0.0/0

    report_status "group set up"
fi

if [[ $key == "" || $keyfile == "" ]]; then
    report_status "set up key"
    which uuidgen 2>/dev/null 1>/dev/null
    if [ $? -eq 0 ]; then
        suffix=`uuidgen`
    else
        suffix=`date +%Y%m%d%H%M%S`
    fi
    key="gentoo-bootstrap_${region}-${suffix}"
    keyfile="${key}.pem"
   
    if [ ! -f $keyfile ]; then
        ec2-add-keypair --region $region $key | sed 1d > $keyfile
        remove_keypair="true"
    fi

    chmod 600 $keyfile
    report_status "key set up"
fi

report_status "architecture = $architecture"
report_status "region = $region"
report_status "group = $group"
report_status "key = $key"
report_status "keyfile = $keyfile"
report_status "AMI_NAME = $AMI_NAME"
report_status "AMI_TYPE = $AMI_TYPE"


report_status "finding latest kernel-id"

latest_kernel="`ec2-describe-images \
--region $region \
--filter image-type=kernel \
--filter manifest-location=*pv-grub* \
--owner amazon \
--filter architecture=${architecture} \
| grep -v 'hd00' \
| awk '{ print $3 \"\t\"  $2 }' \
| sed 's:.*/pv-grub-hd0[^0-9]*::' \
| sort \
| tail -n 1 \
| awk '{ print $2 }'`"

report_status "kernel-id = $latest_kernel"

report_status "finding bootstrap image"

boot_image="`ec2-describe-images \
--region $region \
--owner amazon \
--filter architecture=${architecture} \
--filter image-type=machine \
--filter root-device-type=ebs \
--filter virtualization-type=paravirtual \
--filter kernel-id=${latest_kernel} \
--filter manifest-location=amazon/amzn-ami-pv-* \
| grep '^IMAGE' \
| grep -v 'amazon/amzn-ami-pv-*.*.rc-*' \
| tail -n 1 \
| awk '{ print $2 }'`"

report_status "bootstrap image = $boot_image"

report_status "starting bootstrap instance"

instance="`ec2-run-instances \
--region $region \
$boot_image \
--group $group \
--key $key \
--instance-type $instance_type \
--block-device-mapping '/dev/sdf=:10' \
| grep '^INSTANCE' \
| awk '{ print $2 }'`"

report_status "bootstrap instance = $instance"

report_status "start checking if instance is running"
running_check=0
while [ $running_check -eq 0 ]; do
    sleep 10
    report_status "checking if instance is running (10 second check)"
    let running_check="`ec2-describe-instances \
            --region $region \
            $instance \
            --filter instance-state-name=running \
            | wc -c`"
done

report_status "instance is running"

report_status "getting hostname"

server="`ec2-describe-instances \
--region $region \
$instance \
--filter instance-state-name=running \
| grep '^INSTANCE' \
| awk '{ print $4 }'`"

report_status "hostname = $server"

report_status "Wait 120 seconds, just in case, for server to finish coming up"

sleep 120

report_status "copying files to remote server"

scp -o StrictHostKeyChecking=no -i $keyfile -r ${architecture}/* ${architecture}/.[a-zA-Z0-9]* ec2-user@$server:/tmp
scp -o StrictHostKeyChecking=no -i $keyfile -r remote_gentoo.sh ec2-user@$server:/tmp

report_status "setting remote_gentoo.sh as executable on remote server"

ssh -o StrictHostKeyChecking=no -i $keyfile ec2-user@$server "chmod 755 /tmp/remote_gentoo.sh"
ssh -o StrictHostKeyChecking=no -i $keyfile -t ec2-user@$server "sudo /tmp/remote_gentoo.sh ${architecture} $AMI_TYPE"

report_status "checking if install is done"

stopped_check=0
while [ $stopped_check -eq 0 ]; do
    sleep 60
    report_status "checking if install is done (1 minute check)"
    let stopped_check="`ec2-describe-instances \
            --region $region \
            $instance \
            --filter instance-state-name=stopped \
            | wc -c`"
done

report_status "install is done"

report_status "get volume on which gentoo was installed"

volume="`ec2-describe-volumes \
--region $region \
--filter attachment.instance-id=$instance \
--filter attachment.device=/dev/sdf \
| grep '^VOLUME' \
| awk '{ print $2 } '`"

if [ -z $AMI_TYPE ]; then
    name=${AMI_NAME:-${default_name}-`date --utc +%Y-%m-%d-%H-%M-%S`}
else
    name=${AMI_NAME:-${AMI_TYPE}-${default_name}-`date --utc +%Y-%m-%d-%H-%M-%S`}
fi

report_status "creating snapshot"

snapshot="`ec2-create-snapshot \
--region $region \
$volume \
--description $name \
| awk '{ print $2 }'`"

report_status "checking if snapshot is done"

completed_check=0
while [ $completed_check -eq 0 ]; do
    sleep 60
    report_status "checking if snapshot is done (1 minute check)"
    let completed_check="`ec2-describe-snapshots \
        --region $region \
        $snapshot \
        --filter status=completed \
        | wc -c`"
done

report_status "snapshot is done"

report_status "register image"

if [ $architecture = "i386" ]; then
gentoo_image="`ec2-register \
--region $region \
--name $name \
--description \"${building}\" \
--architecture $architecture \
--kernel $latest_kernel \
--root-device-name /dev/sda1 \
--block-device-mapping \"/dev/sda1=$snapshot\" \
--block-device-mapping '/dev/sda2=ephemeral0' \
--block-device-mapping '/dev/sda3=ephemeral1' \
| awk '{ print $2 }'`"
else
gentoo_image="`ec2-register \
--region $region \
--name $name \
--description \"${building}\" \
--architecture $architecture \
--kernel $latest_kernel \
--root-device-name /dev/sda1 \
--block-device-mapping \"/dev/sda1=$snapshot\" \
--block-device-mapping '/dev/sdb=ephemeral0' \
--block-device-mapping '/dev/sdc=ephemeral1' \
--block-device-mapping '/dev/sdd=ephemeral2' \
--block-device-mapping '/dev/sde=ephemeral3' \
| awk '{ print $2 }'`"
fi

report_status "image-id = $gentoo_image"

gentoo_instance="`ec2-run-instances \
--region $region \
$gentoo_image \
--group $group \
--key $key \
--instance-type t1.micro \
| grep '^INSTANCE' \
| awk '{ print $2 }'`"

report_status "gentoo instance = $gentoo_instance"

report_status "start checking if instance is running"
running_check=0
while [ $running_check -eq 0 ]; do
    sleep 10
    report_status "checking if instance is running (10 second check)"
    let running_check="`ec2-describe-instances \
            --region $region \
            $gentoo_instance \
            --filter instance-state-name=running \
            | wc -c`"
done

report_status "instance is running"

report_status "getting hostname"

server="`ec2-describe-instances \
--region $region \
$gentoo_instance \
--filter instance-state-name=running \
| grep '^INSTANCE' \
| awk '{ print $4 }'`"

report_status "hostname = $server"

report_status "Wait 120 seconds, just in case, for server to finish coming up"

sleep 120

report_status "checking connection"
up_check="`ssh -o StrictHostKeyChecking=no -i $keyfile -t ec2-user@$server 'uname -a' | wc -c`"

if [ $up_check -ne 0 ]; then
    report_status "connection successful"
    report_status "terminating instances"
    ec2-terminate-instances --region $region $instance $gentoo_instance
    report_status "instances terminated"
    if [ $remove_keypair = "true" ]; then
        report_status "deleting keypair"
        ec2-delete-keypair --region $region $key
        rm $keyfile
        report_status "keypair deleted"
    fi
    report_status "gentoo image-id = $gentoo_image"
    report_status "To make the image public run the following command:"
    echo "ec2-modify-image-attribute --region $region $gentoo_image --launch-permission --add all"
else
    report_status "connection failed"
    report_status "stopping instance"
    ec2-stop-instances --region $region $gentoo_instance
    report_status "instance stopped"
    report_status "issue with script, could not verify image. Check instances:"
    report_status "bootstrap instance: $instance"
    report_status "gentoo instance: $gentoo_instance"
    report_status "gentoo image-id = $gentoo_image"
fi

